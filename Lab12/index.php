<?php
    session_start(); 
    include("_footer.html"); 

    if(isset($_POST["submit"]) && !empty($_POST["submit"]))
    {
        if(isset($_POST["nombre"]) && !empty($_POST["nombre"]) && isset($_POST["nombre"]) && !empty($_POST["nombre"]))
        {
            $_POST["nombre"] = htmlspecialchars($_POST["nombre"]);
            $_POST["apellido"] = htmlspecialchars($_POST["apellido"]); 
            $_POST["apodo"] = htmlspecialchars($_POST["apodo"]);
            $_POST["correo"] = htmlspecialchars($_POST["correo"]);            

            $_SESSION['nombre'] = $_POST["nombre"]; 
            $_SESSION['apellido'] = $_POST["apellido"];
            $_SESSION['apodo'] = $_POST["apodo"];
            $_SESSION['correo'] = $_POST["correo"]; 

                       

            $target_dir = "uploads/"; //$target_dir = "uploads/" - specifies the directory where the file is going to be placed
            $target_file = $target_dir.basename($_FILES["fileToUpload"]["name"]); //$target_file specifies the path of the file to be uploaded
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION)); //$imageFileType holds the file extension of the file (in lower case)

            //Check if image file is an actual image or a fake image

            if(isset($_POST["submit"]))
            {
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]); 
                if($check !== false)
                {
                    echo "<br/>"."File is an image - ".$check["mime"]."."; 
                    $uploadOk=1; 
                }
                else
                {
                    echo "<br/><br/>"."File is not an image."; 
                    $uploadOk=0; 
                }

                //Check if file already exists
                if(file_exists($target_file))
                {
                    echo "<br/><br/>"."Sorry, this file already exists."; 
                    $uploadOk=0; 
                }

                //Check file size
                if($_FILES["fileToUpload"]["size"] > 500000)
                {
                    echo "<br/><br/>"."Sorry, your file cannot exceed 500KB"; 
                    $uploadOk=0; 
                }

                //Allow certain file formats
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif")
                {
                    echo "<br/><br/>"."Sorry, file must be JPG, JPEG, PNG or GIF."; 
                    $uploadOk=0; 
                }

                //Check if $uploadOk is set to 0 by an error
                if($uploadOk==0)
                {
                    echo "<br/><br/>"."Sorry, your file could not be uploaded."; 
                }
                //Check if there was no error and try to upload file
                else
                {
                    if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$target_file))
                    {
                        echo "<br/><br/>"."The file ".basename($_FILES["fileToUpload"]["name"])." has been uploaded."; 
                        $_SESSION["img"] = $target_file; 

                    }
                    else
                    {
                        echo "<br/><br/>"."Sorry there was an error uploading your file."; 
                    }
                } exit(); 

            }

        }
        else
        {
            echo "Please fill out all fields."; 
        }
    }


?>