<?php

require_once ('util.php'); 
$result = getFruits(); 

if(mysqli_num_rows($result) > 0)
        {
            echo "<div class='container alert-primary'>"; 
            echo "<table class='table table-striped'>"; 
            echo "<thead class='thead-light'";
            echo "<tr>"; 
            echo "<th scope='col'>Frutas</th>";
            echo "<th scope='col'>Unidades</th>"; 
           // echo "<th scope='col'>Cantidades</th>"; 
            echo "<th scope='col'>Precios</th>"; 
            echo "<th scope='col'>Pais</th>"; 
            echo "</tr>"; 
            echo "</thead>"; 
            echo "<tbody>"; 
    
            while($row = mysqli_fetch_assoc($result))
            {
                echo "<tr>"; 
                echo "<td>" . $row["Name"] . "</td>"; 
                echo "<td>" . $row["Units"] . "</td>"; 
                //echo "<td>" . $row["Quantity"] . "</td>"; 
                echo "<td>" . "$" . $row["Price"] . "</td>"; 
                echo "<td>" . $row["Country"] . "</td>"; 
                echo "</tr>"; 
            }
            
            echo  "</tbody>"; 
            echo "</table>"; 
            echo "</div>"; 
            echo "</div>"; 
        }
?>