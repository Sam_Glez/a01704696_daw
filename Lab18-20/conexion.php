<?php

	function connectDb()
    {
        $servername = "localhost"; 
        $username = "root"; 
        $password = ""; 
        $dbname = "ajax"; 

        
        

        $con = mysqli_connect($servername, $username, $password, $dbname);
        
        //Check connection

        if(!$con)
        {
            die("Connection failed: " . mysqli_connect_error()); 
        }

        return $con; 
    }

    //La variable $mysql es una conexion establecida previamente
    
    function closeDb($mysql)
    {
        mysqli_close($mysql); 
    }

?>