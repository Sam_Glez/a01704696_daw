<?php

    require_once('conexion.php');

    function getFruits()
    {
        $conn = connectDb();
        $sql = "SELECT Name,Units,Quantity, Price, Country FROM Fruit";
        $result = mysqli_query($conn, $sql);

        closeDb($conn);

        return $result;
    }

    //Funcion que regresa los datos de una fruta que tenga en su nombre el parametro
    //Si pones manzana, puede regresar manzana roja, etc. 
    function getFruitsByName($fruit_name)
    {
        $conn = connectDb(); 

        $sql = "SELECT name, units, quantity, price, country FROM Fruit WHERE name LIKE '%" . $fruit_name."%'"; 

        $result = mysqli_query($conn, $sql); 

        closeDb($conn); 

        return $result; 
    }

    function getCheapestFruits($cheap_price)
    {
       $conn = connectDb(); 
       
       $sql = "SELECT name, units, quantity, price, country FROM Fruit WHERE price <= '".$cheap_price."'"; 

       $result = mysqli_query($conn, $sql); 

       closeDb($conn); 

       return $result; 
    }

    function insertFruit($name, $units, $price, $country)
    {
        
        $conn = connectDb(); 

        $sql = "INSERT INTO Fruit (name, units, price, country) VALUES (\"". $name . "\",\"" . $units . "\",\"" . $price . "\",\"" . $country . "\")";

        if(mysqli_query($conn, $sql))
        {
            echo "New record created successfully";
            refresh();  
            closeDb($conn);
            return true; 

        }
        else{
            echo "Error: " . $sql . "<br>" . mysqli_error($conn); 
            closeDb($conn); 
            return false; 
        }

        closeDb($conn); 
    }

    function delete_by_name($name)
        {
            $conn = connectDb(); 

            $sql = "DELETE FROM Fruit WHERE name = '".$name."'"; 

            $result = mysqli_query($conn, $sql); 

            closeDb($conn); 

            return $result;
        }

     function update_by_name($name, $units, $quantity, $price, $country)
     {
        $conn = connectDb(); 

        $sql = "UPDATE Fruit SET name='$name', units='$units', price='$price', country='$country' WHERE name = '$name'"; 

        $result = mysqli_query($conn, $sql); 

        closeDb($conn); 

        return $result; 
     }

     function refresh()
     {
        $result = getFruits(); 
        if(mysqli_num_rows($result) > 0)
        {
            echo "<div class='container alert-primary'>"; 
            echo "<table class='table table-striped'>"; 
            echo "<thead class=''";
            echo "<tr>"; 
            echo "<th scope='col'>Frutas</th>";
            echo "<th scope='col'>Unidades</th>"; 
            //echo "<th scope='col'>Cantidad</th>"; 
            echo "<th scope='col'>Precio</th>"; 
            echo "<th scope='col'>Pais</th>"; 
            echo "</tr>"; 
            echo "</thead>"; 
            echo "<tbody>"; 
    
            while($row = mysqli_fetch_assoc($result))
            {
                echo "<tr>"; 
                echo "<td>" . $row["Name"] . "</td>"; 
                echo "<td>" . $row["Units"] . "</td>"; 
                //echo "<td>" . $row["Quantity"] . "</td>"; 
                echo "<td>" . "$" . $row["Price"] . "</td>"; 
                echo "<td>" . $row["Country"] . "</td>"; 
                echo "</tr>"; 
            }
            
            echo  "</tbody>"; 
            echo "</table>"; 
            echo "</div>"; 
            echo "</div>"; 
    
    
        }
     }





?>