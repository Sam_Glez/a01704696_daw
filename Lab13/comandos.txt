drop TABLE a1704696.Entregan
drop TABLE a1704696.Proveedores
drop TABLE a1704696.Proyectos
drop TABLE a1704696.Materiales

  
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')
DROP TABLE Entregan

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')
DROP TABLE Materiales


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')
DROP TABLE Proveedores

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')
DROP TABLE Proyectos

BULK INSERT a1704696.a1704696.[Materiales]
   FROM 'e:\wwwroot\a1704696\materiales.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

SET DATE dmy

BULK INSERT a1704696.a1704696.[Entregan]
   FROM 'e:\wwwroot\a1704696\entregan.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

BULK INSERT a1704696.a1704696.[Proveedores]
   FROM 'e:\wwwroot\a1704696\proveedores.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

BULK INSERT a1704696.a1704696.[Proyectos]
   FROM 'e:\wwwroot\a1704696\proyectos.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

Select * From Materiales;
Select * From Proveedores;
Select * From Proyectos;
Select * From Entregan;


ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave)
ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)
ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave,RFC,Numero,Fecha)

ALTER TABLE entregan add constraint cfEntreganClave foreign key (Clave) references Materiales(clave);
ALTER TABLE entregan add constraint cfEntreganRfc foreign key (RFC) references Proveedores(RFC);
ALTER TABLE entregan add constraint cfEntreganNumero foreign key (Numero) references Proyectos(Numero);

ALTER TABLE entregan add constraint cantidad check (cantidad > 0) ;
ALTER TABLE Materiales add constraint costoMin check (Costo > 0);

