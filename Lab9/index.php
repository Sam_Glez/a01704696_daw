<?php

	include("_header.html");


	
	function promedio(array $arr){
		$suma = 0;
		$indice = sizeof($arr);
		$respuesta=0;

		$suma=array_sum($arr);
		

		$respuesta = $suma/$indice;
		$respuesta=round($respuesta, 3);
		echo "El promedio es: $respuesta <br>";
	}

	function mediana (array $arr){
		
		sort($arr);
		$num= sizeof($arr);
		$mid= $num/2;
		

		if($num % 2 == 0){
			
			$val=promedio([$arr[$mid-1],$arr[$mid]]);
			echo "La mediana es $val <br>";
		}else{
			$val=$arr[floor($mid)];
			echo "La mediana es $val <br>";
		}

	}

	function muestraarr (array $arr){

		echo implode(', ', $arr) . "<br>";

		promedio($arr);

		mediana($arr);

		sort($arr);
		echo implode(', ', $arr) . "<br>";

		rsort($arr);

		echo implode(', ', $arr) . "<br>";

	}

	function creatabla($num){

		echo "

			<table>
				<thead>
					<tr>
						<th> valor </th>
						<th> valor al cuadrado </th>
						<th> valor al cubo </th>
					</tr>
				</thead>

				<tbody>
		";

		for ($i=1; $i <= $num; $i++){
			$i2=$i*$i;
			$i3=$i*$i*$i;
			

			echo "
				<tr>
					<td>{$i}</td>
					<td>{$i2}</td>
					<td>{$i3}</td>
					
				</tr>
			";
		}

		echo "</tbody </table> <br>";


	}
	
	$max = rand(10,14);
    $arregloNumeros = array($max);
    for($i = 0; $i < $max; $i++)
    {
        $arregloNumeros[$i] = rand(0,15);
    }


    function problema(array $arr){
    	sort($arr);
    	$num= sizeof($arr);
    	$mayor=0;


    	for($i=0; $i < $num; $i++){
    		if($mayor < $arr[$i]){
    			$mayor = $arr[$i];
    		}
    	}
    	echo "Se mostrar el valor mayor del arreglo recibido <br>";
    	echo "El valor mayor del arreglo es $mayor";

    }
	
	?>
	<div class="container alert alert-warning table"><?php creatabla(10) ?></div>
	<div class="container alert alert-success"><?php muestraarr($arregloNumeros)?></div>	
	<div class="container alert alert-dark"><?php problema($arregloNumeros)?></div>	

	<div class="container alert alert-light">
	<div class="row">
		<h5>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h5>
		<h6> Muestra las características del sistema en el que está corriendo el interpretador de PHP, así como las variables locales y los módulos activados. Me llama la atención que puedas desactivar cada modulo como CURL, Hashing, FTP e incluso el calendario</h6>
	</div>

	<div class="row">
		<h5>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h5>
		<h6> Agregar métodos de seguridad para evitar ataques</h6>
	</div>

	<div class="row">
		<h5>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h5>
		<h6>Al recibir un request de parte del cliente, el servidor pre-procesa el código y genera y manda de regreso algo legible por el navegador</h6>
	</div>
</div>
	
	<?php 

	include("_footer.html"); 
?>

