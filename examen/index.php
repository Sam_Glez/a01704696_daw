

<!DOCTYPE html>
<html>
<head>
<!-- Para hacerlo mobile friendly -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Tabla dinamica</title>
	<link rel="stylesheet" type="text/css" href="librerias/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="librerias/alertifyjs/css/themes/default.css">
  <link rel="stylesheet" type="text/css" href="librerias/select2/css/select2.css">
  <link rel="stylesheet" href="../Prototipo_1/Prototipo_1.css">

	<script src="librerias/jquery-3.2.1.min.js"></script>
  <script src="librerias/bootstrap/js/bootstrap.js"></script>
	<script src="librerias/alertifyjs/alertify.js"></script>
  <script src="librerias/select2/js/select2.js"></script>
  <script src="js/funciones.js"></script>
  
</head>

<body>

  <header>
           
    </header>


    <div class="container formulario">
        <h2>Zombies</h2>
        <br>
        
    </div>
    <br>

  <div class="container formulario">
    
    <div id="tabla"></div>
  </div>

  <!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="modalNuevo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form method="POST">
          <label>Nombre Empleado</label>
          <input type="text" name="" id="nombre" class="form-control input-sm">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="guardanuevo">Agregar</button>
          </form>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="modalEdicion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualiza informacion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST">
          
          <label>Infeccion</label>
          <input type="checkbox" name="" id="infeccion" class="btn btn-warning">
          <br>
          <label>Desorientacion</label>
          <input type="checkbox" name="" id="desorientacion" class="btn btn-warning">
          <br>
          <label>Violencia</label>
          <input type="checkbox" name="" id="violencia" class="btn btn-warning">
          <br>
          <label>Desmayo</label>
          <input type="checkbox" name="" id="desmayo" class="btn btn-warning">
          <br>
          <label>Transformacion</label>
          <input type="checkbox" name="" id="transformacion" class="btn btn-warning">
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal" id="actualizadatos">Actualizar</button>
        
      </div>
    </div>
  </div>
</div>

</body>
</html>







<script type="text/javascript">
  $(document).ready(function(){
    $('#tabla').load('componentes/tabla.php');
    //$('#buscador').load('componentes/buscador.php');
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#guardanuevo').click(function(){
      
      nombre=$('#nombre').val();

      agregardatos(nombre);

    });


    $('#actualizadatos').click(function(){
      nombre=$('#nombre').val();
      infeccion=$('#infeccion').val();
      desorientacion=$('#desorientacion').val();
      violencia=$('#violencia').val();
      desmayo=$('#desmayo').val();
      transformacion=$('#transformacion').val();
      actualizaDatos(nombre, infeccion, desorientacion, violencia, desmayo, transformacion);
    });


  });
</script>

