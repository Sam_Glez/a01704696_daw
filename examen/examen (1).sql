-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-10-2020 a las 01:36:51
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertar_zombie` (IN `nombre_zombie` VARCHAR(50))  NO SQL
BEGIN
	INSERT INTO zombies (Id, Nombre) values (NULL, nombre_zombie);

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `Id` int(11) DEFAULT NULL,
  `Infeccion` timestamp NULL DEFAULT current_timestamp(),
  `Desorientacion` timestamp NULL DEFAULT current_timestamp(),
  `Violencia` timestamp NULL DEFAULT current_timestamp(),
  `Desmayo` timestamp NULL DEFAULT current_timestamp(),
  `Transformacion` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`Id`, `Infeccion`, `Desorientacion`, `Violencia`, `Desmayo`, `Transformacion`) VALUES
(1, '2020-10-01 20:45:16', '2020-10-08 20:45:16', '2020-10-28 21:45:29', '2020-10-23 20:45:16', '2020-10-28 21:45:29'),
(2, '2020-10-19 20:45:33', '2020-10-28 21:45:40', '2020-10-28 21:45:40', '2020-10-22 20:45:33', '2020-10-28 21:45:40'),
(3, '2020-10-28 21:45:47', '2020-10-28 21:45:47', '2020-10-28 21:45:47', '2020-10-28 21:45:47', '2020-10-25 21:45:43'),
(4, '2020-10-08 20:46:07', '2020-10-28 21:46:13', '2020-10-28 21:46:13', '2020-10-28 21:46:13', '2020-10-28 21:46:13'),
(5, '2020-10-08 20:46:07', '2020-10-28 21:52:32', '2020-10-28 21:52:32', '2020-10-28 21:52:32', '2020-10-28 21:52:32'),
(6, '2020-10-08 20:46:07', '2020-10-28 21:52:54', '2020-10-28 21:52:54', '2020-10-28 21:52:54', '2020-10-28 21:52:54'),
(7, '2020-10-08 20:46:07', '2020-10-28 21:53:02', '2020-10-28 21:53:02', '2020-10-28 21:53:02', '2020-10-28 21:53:02'),
(8, '2020-10-08 20:46:07', '2020-10-28 21:53:09', '2020-10-28 21:53:09', '2020-10-28 21:53:09', '2020-10-28 21:53:09'),
(9, '2020-10-08 20:46:07', '2020-10-28 21:53:41', '2020-10-28 21:53:41', '2020-10-28 21:53:41', '2020-10-28 21:53:41'),
(10, '2020-10-28 21:56:11', '2020-10-28 21:56:11', '2020-10-28 21:56:11', '2020-10-28 21:56:11', '2020-10-28 21:56:11'),
(11, '2020-10-28 21:56:44', '2020-10-28 21:56:44', '2020-10-28 21:56:44', '2020-10-28 21:56:44', '2020-10-28 21:56:44'),
(30, NULL, NULL, NULL, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL),
(NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zombies`
--

CREATE TABLE `zombies` (
  `Id` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `zombies`
--

INSERT INTO `zombies` (`Id`, `Nombre`) VALUES
(1, 'Eduardo'),
(2, 'Ricardo'),
(3, 'Javier'),
(4, 'Luis'),
(5, 'Tony'),
(6, 'Roberto'),
(7, 'Jaime'),
(8, 'Tomas'),
(9, 'Saul'),
(10, 'Raul'),
(11, 'Giovany'),
(29, 'Enrique'),
(30, 'Ignacio'),
(31, 'Jaime'),
(32, 'Roberto'),
(33, 'Luis'),
(37, 'Roberto'),
(38, 'Javier'),
(39, 'Enrique'),
(40, 'Roberto');

--
-- Disparadores `zombies`
--
DELIMITER $$
CREATE TRIGGER `agregar_estado` AFTER INSERT ON `zombies` FOR EACH ROW INSERT INTO estado VALUES ( Id, NULL, NULL, NULL, NULL, NULL)
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `zombies`
--
ALTER TABLE `zombies`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `zombies`
--
ALTER TABLE `zombies`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
