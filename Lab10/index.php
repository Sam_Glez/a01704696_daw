<?php
	
	include("_header.html");

	$error="";
	$flag=FALSE;

	
	if(isset($_POST["submit"])){
		$name=htmlspecialchars($_POST["name"]);
		$email=htmlspecialchars($_POST["email"]);
		$phone=htmlspecialchars($_POST["phone"]);
		$cumpleanos=htmlspecialchars($_POST["cumpleanos"]);


		if (!preg_match("/^[a-zA-Z0-9\- ]*$/",$name)) {
            $error = $error."<br>"."Invalid Character in name";
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = $error."<br>"."Invalid email format";
        }
        if(!preg_match("/^[0-9]{10}$/", $phone)) {
            $error = $error."<br>"."Phone is invalid";
        }
        if($error != ""){
            $flag = TRUE;

        }
	}

	



	
	include("form.html");

		
	include("_footer.html");
?>