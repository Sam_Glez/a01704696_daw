USE [a1704696]
GO
CREATE TABLE [a1704696].[Entregan](
	[Clave] [numeric](5, 0) NULL,
	[RFC] [char](13) NULL,
	[Numero] [numeric](5, 0) NULL,
	[Fecha] [datetime] NULL,
	[Cantidad] [numeric](8, 2) NULL
) ON [PRIMARY]
GO

CREATE TABLE [a1704696].[Materiales](
	[Clave] [numeric](5, 0) NULL,
	[Descripcion] [varchar](50) NULL,
	[Costo] [numeric](8, 2) NULL
) ON [PRIMARY]
GO

CREATE TABLE [a1704696].[Proveedores](
	[RFC] [char](13) NULL,
	[RazonSocial] [varchar](50) NULL
) ON [PRIMARY]
GO

CREATE TABLE [a1704696].[Proyectos](
	[Numero] [numeric](5, 0) NULL,
	[Denominacion] [varchar](50) NULL
) ON [PRIMARY]
GO
USE [a1704696]
GO
