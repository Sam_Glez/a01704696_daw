BULK INSERT a1704696.a1704696.[Materiales]
   FROM 'e:\wwwroot\a1704696\materiales.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

BULK INSERT a1704696.a1704696.[Entregan]
   FROM 'e:\wwwroot\a1704696\entregan.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

BULK INSERT a1704696.a1704696.[Proveedores]
   FROM 'e:\wwwroot\a1704696\proveedores.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

BULK INSERT a1704696.a1704696.[Proyectos]
   FROM 'e:\wwwroot\a1704696\proyectos.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

Select * From Materiales;
Select * From Proveedores;
Select * From Proyectos;
Select * From Entregan;

